all: main.o class_Pole.o class_Uzivatel.o
	g++ main.o class_Pole.o class_Uzivatel.o -o Gore-dole,_levo-desno

main.o: main.cpp
	g++ -c main.cpp

class_Pole.o: class_Pole.cpp class_Pole.h
	g++ -c class_Pole.cpp

class_Uzivatel.o: class_Uzivatel.cpp class_Uzivatel.h
	g++ -c class_Uzivatel.cpp

clean:
	rm *.o
	rm Gore-dole,_levo-desno
	rm Vysledky.txt
