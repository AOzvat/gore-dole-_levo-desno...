#include <iostream>
#include <fstream>
#include "string.h"
#include "class_Pole.h"
#include "class_Uzivatel.h"

void Uzivatel::zadaj_meno (){
	//uzivatel si zada meno
	
	cout << "Zadajte Vase meno alebo primenie: " << endl;
	getchar();
	getline (cin, this->meno);
}

void Uzivatel::krok (Pole *pole){
	//vykona sa jeden krok v hre
	
	char nasledovny_krok;
	bool kontrola = false;

	pole->vykreslenie();
	cout << endl;

	//uzivatel zada svoj pohyb
	//kontrola ci sme zadali nejaky smer, alebo len nahodny znak
	cout << "Spravte krok." << endl;
	cout << "Hore - w, Dolu - s, Lavo - a, Pravo - d" << endl;
	cout << "Koniec - k" << endl;

	while (!kontrola){
		nasledovny_krok = cin.get();
		
		this->koniec(nasledovny_krok);

		if (nasledovny_krok == 'w' || nasledovny_krok == 's' || nasledovny_krok == 'd' || nasledovny_krok == 'a' || zakonc == true){
			kontrola = true;
		}
		else{
			kontrola = false;
		}
	}

	//vykona sa pohyb
	if (!zakonc){
		klavesnicovy_pohyb klaves_nasledovny_pohyb = (klavesnicovy_pohyb) nasledovny_krok;
		pole->posun (klaves_nasledovny_pohyb);
		cout << endl;
	}
}

void Uzivatel::zapis (Pole *pole){
	//vykona sa zapis do suboru o predchadzajucej hre
	
	fstream subor ("Vysledky.txt", fstream::out | fstream::app);

	if (!subor.is_open()){
		cout << "Nepodarilo sa otvorit subor!" << endl;
	}
	else{
		subor << "Meno:          " << meno << endl;
		subor << "Rozmer pola:   " << pole->povedz_size() << "x" << pole->povedz_size() << endl;
		
		if (!zakonc){
			subor << "Pocet pokusov: " << pokus << endl << endl;
		}
		else{
			subor << "Hru ste ukoncili, stlacenim tlacidla" << endl;
			subor << "k-koniec, po " << pokus << " pokusoch." << endl << endl;
		}
		
		subor.close();
	}
}

void Uzivatel::hra (Pole *pole){
	//cast potrebna pre hru, aby sa volala jedna funkcia

	pokus = 0;
	zakonc = false;

	//volanie funkcie krok, kim nije plocha vo finalnej podobe
	while (!pole->su_rovnake()){
		this->krok(pole);

		//pricita sa pokus, pre zapis do suboru	

		if (pole->povedz_mimo()){
			pokus = pokus;
		}
		else {
			pokus = pokus + 1;
		}

		//ak je plocha vo finalnej podobe, vykona sa zadavanie mena a zapis do suboru
		if (pole->su_rovnake() || zakonc){
			pole->vykreslenie();

			cout << endl << "KONIEC HRY!" << endl << endl;
			this->zadaj_meno();

			if (zakonc){
				if (pokus != 0){
					pokus = pokus - 1;
				}
			}

			this->zapis(pole);

			if (!zakonc){
				cout << endl << "Spravili ste " << pokus << " krokov." << endl << endl;
			}
			else{
				cout << endl << "Hru ste ukoncili, stlacenim tlacidla" << endl;
			  	cout <<	"k-koniec, po " << pokus << " krokoch." << endl << endl;
			}
			pole->vyrovnaj();
		}
	}

	this->zaver();
}

void Uzivatel::koniec (char znak){
	//ak uzivatel zada znak pre koniec hre

	if (znak == 'k'){
		zakonc = true;
	}
}

void Uzivatel::zaver (){
	//vypis na zaver

	cout << endl << "###########################################" << endl << endl;
	cout << "            DO SKOREHO VIDENIA!            " << endl << endl;
	cout << "###########################################" << endl << endl;
}
