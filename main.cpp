#include "class_Pole.h"
#include "class_Uzivatel.h"

using namespace std;

int main (void){
	Pole pole;
	Uzivatel uzivatel;

	pole.vykonaj();

	uzivatel.hra(&pole);

	return 0;
}
