#ifndef CLASS_UZIVATEL_H
#define CLASS_UZIVATEL_H

#include <iostream>
#include "string.h"
#include "class_Pole.h"

using namespace std;

class Uzivatel{
	private:
		string meno;
		int pokus;
		bool zakonc;

	public:
		void zadaj_meno ();
		void krok (Pole *pole);
		void zapis (Pole *pole);
		void hra (Pole *pole);
		void koniec (char znak);
		void zaver ();
};

#endif /* include guard CLASS_UZIVATEL_H */
