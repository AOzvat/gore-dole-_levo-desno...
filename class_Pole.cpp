#include <iostream>
#include <string.h>
#include <sstream>
#include <ctime>
#include <cstdlib>
#include "class_Pole.h"

using namespace std;

void Pole::uvod_a_instrukcie (){
	//uvod a instrukcie k hre

	cout << endl << "###########################################" << endl << endl;
	cout << "          GORE-DOLE, LEVO-DESNO..          " << endl << endl;
	cout << "###########################################" << endl << endl << endl;

	cout << "Ak mate prilis vela volneho casu, tak ste  " << endl;
	cout <<	"na spravnom mieste. Tato aplikacia si Vas  " << endl;
	cout <<	"obmota okolo prsta a nedovoli Vam prestat. " << endl;
	cout << "A urcite sa naucite abecedu. :)            " << endl << endl << endl;
	
	cout << "###########################################" << endl << endl;
	cout << "Ciel hry:                                  " << endl;
	cout << "Usporiadat pismenka podla abecedy,         " << endl;
	cout <<	"posuvanim policok:                         " << endl;
	cout <<	"hore, dolu, dolava a doprava.              " << endl << endl;
	cout << "###########################################" << endl << endl << endl;

	//zadanie velkosti hracej plochy
	cout << "Vyberte si velkost hracieho pola:          " << endl;
	cout << "3 -> 3x3, 4 -> 4x4, 5 -> 5x5               " << endl;
	cin >> size;

	//plocha musi byt vacsia ako 2, cize 3 alebo viac
	while (size < 3 || size > 5){
		cout << "Musite vniest cislo vacsie ako 3,          " << endl;
		cout << "ale mensie ako 5.                          " << endl;
		cin >> size;
	}

	cout << endl << endl;

	cout << "Posuvat policka budete klavesnicou:        " << endl;
	cout << "Hore - w, Dolu - s, Lavo - a, Pravo - d    " << endl << endl;
	cout << "Ak budete chciet ukoncit hru bez           " << endl;
	cout << "dosiahnutia finalnej podobe, stlacte:      " << endl;
	cout << "Koniec - k                                 " << endl << endl;
	cout << "               VELA STASTIA!               " << endl << endl << endl;
	cout << "###########################################" << endl << endl;
}

void Pole::inicializacia (){
	//vytvori sa hracia plocha vo finalnej podobe
	//a este jedna finalna podoba pre porovnavanie
	
	if ((plocha = new string *[size]) == NULL){
		return;
	}
	if ((blok = new string [size*size]) == NULL){
		return;
	}

	if ((finalna_plocha = new string *[size]) == NULL){
		return;
	}
	if ((finalny_blok = new string [size*size]) == NULL){
		return;
	}

	for (int i = 0; i < size; i++){
		plocha [i] = &blok [i*size];
	}

	for (int i = 0; i < size; i++){
		finalna_plocha [i] = &finalny_blok [i*size];
	}

	char pismeno = 'A';
	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			if ((i == size-1) && (j == size-1)){
				plocha [i][j] = "   ";
				finalna_plocha [i][j] = plocha [i][j];
			}
			else{
				stringstream ss;
				string string_pismeno;
				ss << pismeno;
				ss >> string_pismeno;
				plocha [i][j] = " " + string_pismeno + " ";
				finalna_plocha [i][j] = plocha [i][j];
				pismeno++;
			}
		}
	}
}

void Pole::vykreslenie (){
	//vykresli hraciu plochu
	
	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			cout << plocha [i][j];
		}
		cout << endl;
	}
}

void Pole::pomiesanie (){
	//pomiesa hracie policka
	
	srand ((unsigned)time (0));
	
	for (int i = 0; i < 1000; i++){
		int smer = (rand() % 4);
		switch (smer){
			case (0):{
				   this->posun (hore);
				   break;
				   }
			case (1):{
				   this->posun (dolu);
				   break;
				   }
			case (2):{
				   this->posun (lavo);
				   break;
				   }
			case (3):{
				   this->posun (pravo);
				   break;
				   }
		}	
	}
}

void Pole::posun (klavesnicovy_pohyb klavesnica){
	//posunie policko tam kam uzivatel potrebuje
	
	mimo = false;

	this->zisti_prazdno_miesto ();

	policko_riadok = prazdny_riadok;
	policko_stlpec = prazdny_stlpec;

	switch (klavesnica){
		case (hore):{
					policko_riadok = prazdny_riadok + 1;
					break;
					}
		case (dolu):{
					policko_riadok = prazdny_riadok - 1;
					break;
					}
		case (lavo):{
					policko_stlpec = prazdny_stlpec + 1;
					break;
					}
		case (pravo):{
					 policko_stlpec = prazdny_stlpec - 1;
					 break;
					 }
	}

	//kontrola, aby pohyb bov v hraniciach a premiestnenie
	if (policko_riadok >= 0 && policko_riadok < size && policko_stlpec >= 0 && policko_stlpec < size){
		plocha [prazdny_riadok][prazdny_stlpec] = plocha [policko_riadok][policko_stlpec];
		plocha [policko_riadok][policko_stlpec] = "   ";
	}
	else{
		if (v_hre){
			this->hlaska();
			mimo = true;
		}
	}
}

void Pole::zisti_prazdno_miesto (){
	//zisti kde je prazdne policko prave teraz
	
	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			if (plocha [i][j] == "   "){
				prazdny_riadok = i;
				prazdny_stlpec = j;
			}
		}
	}
}

int Pole::povedz_size (){
	//vrati velkost pola
	
	return size;
}

void Pole::vykonaj (){
	//vykonaju sa prikazi, aby hra fungovala
	
	v_hre = false;

	this->uvod_a_instrukcie();

	cout << endl << "Vas ciel je dosiahnut:" << endl << endl;
	this->inicializacia();
	this->vykreslenie();

	cout << endl << endl << "Ked Vam dame toto:" << endl;
	this->pomiesanie();
	cout << endl;

	v_hre = true;
}

bool Pole::su_rovnake (){
	//vyhodnoti sa ci sa hracia plocha rovna finalnej podobe hracej plochy
	
	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			if (plocha [i][j] != finalna_plocha [i][j]){
				return false;
			}
		}
	}

	return true;
}

Pole::~Pole (){
	//dealokacia
	
	delete []plocha;
	delete []blok;
	delete []finalna_plocha;
	delete []finalny_blok;
}

void Pole::vyrovnaj (){
	//pri ukonceni bez dosiahnutia finalnej podobe
	//je potrebe dosiahnut finalnu podobu aj tak
	//stane sa to tu

	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			plocha [i][j] = finalna_plocha [i][j];
		}
	}
}

void Pole::hlaska (){
	//iba hlaska v procedure krok

	cout << endl << "Spravili ste krok mimo hracej plochy." << endl;
}

bool Pole::povedz_mimo (){
	//vrati hodnotu mimo

	return mimo;
}
