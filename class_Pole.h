#ifndef CLASS_POLE_H
#define CLASS_POLE_H

#include <iostream>
#include <string.h>
#include <sstream>
#include <ctime>
#include <cstdlib>

using namespace std;

enum klavesnicovy_pohyb {
	hore = 'w',
	dolu = 's',
	lavo = 'a',
	pravo = 'd'
};

class Pole{
	private:
		string **plocha;
		string *blok;
		string **finalna_plocha;
		string *finalny_blok;
		int size;
		int prazdny_riadok;
		int prazdny_stlpec;
		int policko_riadok;
		int policko_stlpec;
		bool v_hre;
		bool mimo;

	public:
		void uvod_a_instrukcie ();
		void inicializacia ();
		void vykreslenie ();
		void pomiesanie ();
		void posun (klavesnicovy_pohyb klavesnica);
		void zisti_prazdno_miesto ();	
		int povedz_size ();
		void vykonaj ();
		bool su_rovnake ();
		~Pole();
		void vyrovnaj ();
		void hlaska ();
		bool povedz_mimo ();
};

#endif /* include guard CLASS_POLE_H */
